const getTableData = (req, res, db) => {
  db.select("*")
    .from("taches")
    .then((items) => {
      if (items.length) {
        res.json(items);
      } else {
        res.json({ dataExists: "false" });
      }
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

const postTableData = (req, res, db) => {
  const { description, date_fin, importance, etat } = req.body;
  const added = new Date();
  db("taches")
    .insert({ description, date_fin, importance, etat, added })
    .returning("*")
    .then((item) => {
      res.json(item);
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

const putTableData = (req, res, db) => {
  const { id, description, date_fin, importance, etat } = req.body;
  db("taches")
    .where({ id })
    .update({ description, date_fin, importance, etat })
    .returning("*")
    .then((item) => {
      res.json(item);
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

const deleteTableData = (req, res, db) => {
  const { id } = req.body;
  db("taches")
    .where({ id })
    .del()
    .then(() => {
      res.json({ delete: "true" });
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

module.exports = {
  getTableData,
  postTableData,
  putTableData,
  deleteTableData,
};
