//Mongo
//Définition des modules
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

//Connexion à la base de donnée
mongoose
  .connect("mongodb://mongod:27017/authtable")
  .then(() => {
    console.log("Connected to mongoDB");
  })
  .catch((e) => {
    console.log("Error while DB connecting");
    console.log(e);
  });

//On définit notre objet express nommé app
const app = express();

//Body Parser
const urlencodedParser = bodyParser.urlencoded({
  extended: true,
});
app.use(urlencodedParser);

app.use(bodyParser.json());

//Définition des CORS
app.use(function (req, res, next) {
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

//Définition du routeur
const router = express.Router();
app.use("/user", router);
app.get("/", (req, res) => res.send("Mongo ok"));
require(__dirname + "/controllers/userController")(router);

//Définition et mise en place du port d'écoute
const port = 8800;
app.listen(port, () => console.log(`Tourne sur le port : ${port}`));

// postgres
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");

// Connection à la base de données
var db = require("knex")({
  client: "pg",
  connection: {
    host: "postgresqlhost",
    user: "postgres",
    password: "mypassword",
    database: "projetnosqldb",
  },
});

const main = require("./controllers/main");

const app2 = express();

// Port accessible
const whitelist = ["http://localhost:3000"];

const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

app2.use(helmet());
app2.use(cors(corsOptions));
app2.use(bodyParser.json());
app2.use(morgan("combined"));

// Routes
app2.get("/", (req, res) => res.send("Postgres ok"));
app2.get("/crud", (req, res) => main.getTableData(req, res, db));
app2.post("/crud", (req, res) => main.postTableData(req, res, db));
app2.put("/crud", (req, res) => main.putTableData(req, res, db));
app2.delete("/crud", (req, res) => main.deleteTableData(req, res, db));

// Connexion au serveur
app2.listen(process.env.PORT || 3001, () => {
  console.log(`Tourne sur le port ${process.env.PORT || 3001}`);
});