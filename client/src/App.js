import React, { Component } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import ModalForm from "./Components/Modals/Modal";
import DataTable from "./Components/Tables/DataTable";
import API from "./utils/API";
import axios from "axios";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
  }

  getItems() {
    axios
      .get("http://localhost:3001/crud")
      .then((response) => {
        console.log(response.data);
        if (
          response.data.dataExists === "false" ||
          response.data.dataExists === false
        ) {
          this.setState({ items: [] });
        } else if (response.data.dataExists === undefined) {
          this.setState({ items: response.data });
        } else {
          this.setState({ items: response.data });
        }
      })
      .catch((err) => console.log(err));
  }

  // getItems() {
  //   fetch("http://localhost:3000/crud")
  //     .then((response) => {
  //       console.log(response);
  //       response.json();
  //     })
  //     .then((items) => {
  //       console.log(items);
  //       if (items.dataExists === "false") {
  //         this.setState({ items: [] });
  //       } else if (items.dataExists === undefined) {
  //         this.setState({ items: items });
  //       }
  //     })
  //     .catch((err) => console.log(err));
  // }

  addItemToState = (item) => {
    console.log(item);
    this.setState((prevState) => ({
      items: [...prevState.items, item],
    }));
  };

  updateState = (item) => {
    const itemIndex = this.state.items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...this.state.items.slice(0, itemIndex),
      item,
      ...this.state.items.slice(itemIndex + 1),
    ];
    this.setState({ items: newArray });
  };

  deleteItemFromState = (id) => {
    const updatedItems = this.state.items.filter((item) => item.id !== id);
    this.setState({ items: updatedItems });
  };

  componentDidMount() {
    this.getItems();
  }

  disconnect = () => {
    API.logout();
    window.location = "/";
  };

  render() {
    console.log(this.state.items);
    return (
      <>
        <Container fluid className="App">
          <Row>
            <Col>
              <h1 style={{ margin: "20px 0" }}>Projet noSQL - Tâches</h1>
            </Col>
            <Col style={{ marginLeft: "85%" }}>
              <Button onClick={this.disconnect} type="submit" color="secondary">
                Se déconnecter
              </Button>
            </Col>
          </Row>
          <Row>
            <Col>
              <DataTable
                items={this.state.items}
                updateState={this.updateState}
                deleteItemFromState={this.deleteItemFromState}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ModalForm
                buttonLabel="Ajouter un élément"
                addItemToState={this.addItemToState}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default App;
