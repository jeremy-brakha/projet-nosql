import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import { App } from "./App.js";
import { Login } from "./Components/Login/login.js";
import { Signup } from "./Components/Signup/signup.js";
import { PrivateRoute } from "./Components/PrivateRoute.js";
import "./App.css";

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: null,
    };
  }

  render() {
    return (
      <div className="App">
        <div className="App-content">
          <Routes>
            <Route exact path="/" element={<Login />} />
            <Route exact path="/signup" element={<Signup />} />
            <Route exact path="/" element={<PrivateRoute />}>
              <Route exact path="/app" element={<App />} />
            </Route>
          </Routes>
        </div>
      </div>
    );
  }
}
export default Test;
