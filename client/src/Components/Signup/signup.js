import React from "react";
import { Button, FormGroup, Input, Alert } from "reactstrap";
import { Label } from "reactstrap";
import API from "../../utils/API";

export class Signup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      cpassword: "",
      showError: false,
    };
    this.onDismiss = this.onDismiss.bind(this);
  }
  send = async () => {
    const { email, password, cpassword } = this.state;
    if (!email || email.length === 0) return;
    if (!password || password.length === 0 || password !== cpassword) {
      this.setState({ showError: true });
      return;
    }
    try {
      const { data } = await API.signup(email, password);
      localStorage.setItem("token", data.token);
      window.location = "/app";
    } catch (error) {
      console.error(error);
    }
  };
  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  onDismiss() {
    this.setState({ showError: false });
  }
  render() {
    const { email, password, cpassword, showError } = this.state;
    return (
      <div className="Login">
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            autoFocus
            type="email"
            id="email"
            value={email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label for="password">Mot de passe</Label>
          <Input
            value={password}
            onChange={this.handleChange}
            type="password"
            id="password"
          />
        </FormGroup>
        <FormGroup>
          <Label for="cpassword">Confirmer le mot de passe</Label>
          <Input
            value={cpassword}
            onChange={this.handleChange}
            type="password"
            id="cpassword"
          />
        </FormGroup>
        <Button
          color="primary"
          onClick={this.send}
          block
          type="submit"
          style={{ marginBottom: "5px" }}
        >
          Inscription
        </Button>
        <Button
          color="secondary"
          onClick={() => (window.location = "/")}
          block
          style={{ marginBottom: "5px" }}
        >
          Retour
        </Button>
        <Alert color="danger" isOpen={showError} toggle={this.onDismiss}>
          Veuillez vérifier vos informations.
        </Alert>
      </div>
    );
  }
}
