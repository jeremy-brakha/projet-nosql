import React, { Component } from "react";
import { Table, Button } from "reactstrap";
import ModalForm from "../Modals/Modal";
import Swal from "sweetalert2";
class DataTable extends Component {
  deleteItem = (id) => {
    Swal.fire({
      icon: "warning",
      iconColor: "#808080",
      title: "Etes-vous sûr ?",
      text: "Vous ne pourrez pas revenir en arrière !",
      showCancelButton: true,
      confirmButtonText: "Oui, supprimer",
      confirmButtonColor: "#0d6efd",
      cancelButtonText: "Annuler",
    }).then((result) => {
      if (result.isConfirmed) {
        fetch("http://localhost:3001/crud", {
          method: "delete",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id,
          }),
        })
          .then((response) => response.json())
          .then((item) => {
            this.props.deleteItemFromState(id);
            Swal.fire({
              icon: "success",
              title: "Deleted!",
              text: `Data has been deleted.`,
              showConfirmButton: false,
              timer: 1500,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };

  render() {
    const items =
      this.props.items ? (
        this.props.items.map((item) => {
          return (
            <tr key={item.id}>
              <th scope="row">{item.id}</th>
              <td>{item.description}</td>
              <td>{item.date_fin}</td>
              <td>{item.importance}</td>
              <td>{item.etat}</td>
              <td>
                <div style={{ width: "110px" }}>
                  <ModalForm
                    buttonLabel="Éditer"
                    item={item}
                    updateState={this.props.updateState}
                  />{" "}
                  <Button
                    color="danger"
                    onClick={() => this.deleteItem(item.id)}
                  >
                    Supprimer
                  </Button>
                </div>
              </td>
            </tr>
          );
        })
      ) : (
        <></>
      );

    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Description</th>
            <th>Date de fin</th>
            <th>Importance</th>
            <th>Etat</th>
          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    );
  }
}

export default DataTable;
