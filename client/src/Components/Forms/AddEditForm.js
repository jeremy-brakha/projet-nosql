import React from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

class AddEditForm extends React.Component {
  state = {
    id: 0,
    description: "",
    date_fin: "",
    importance: "",
    etat: "",
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  submitFormAdd = (e) => {
    console.log(this.state);
    e.preventDefault();
    fetch("http://localhost:3001/crud", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        description: this.state.description,
        date_fin: this.state.date_fin,
        importance: this.state.importance,
        etat: this.state.etat,
      }),
    })
      .then((response) => response.json())
      .then((item) => {
        console.log(item);
        console.log(Array.isArray(item));
        if (Array.isArray(item)) {
          this.props.addItemToState(item[0]);
          this.props.toggle();
        } else {
          console.log("failure");
        }
      })
      .catch((err) => console.log(err));
  };

  submitFormEdit = (e) => {
    e.preventDefault();
    fetch("http://localhost:3001/crud", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: this.state.id,
        description: this.state.description,
        date_fin: this.state.date_fin,
        importance: this.state.importance,
        etat: this.state.etat,
      }),
    })
      .then((response) => response.json())
      .then((item) => {
        if (Array.isArray(item)) {
          this.props.updateState(item[0]);
          this.props.toggle();
        } else {
          console.log("failure");
        }
      })
      .catch((err) => console.log(err));
  };

  componentDidMount() {
    // if item exists, populate the state with proper data
    if (this.props.item) {
      const { id, description, date_fin, importance, etat } = this.props.item;
      this.setState({ id, description, date_fin, importance, etat });
    }
  }

  render() {
    const options = [1, 2, 3];
    return (
      <Form
        onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}
      >
        <FormGroup>
          <Label for="description">Description</Label>
          <Input
            type="text"
            name="description"
            id="description"
            onChange={this.onChange}
            value={
              this.state.description === null ? "" : this.state.description
            }
          />
        </FormGroup>
        <FormGroup>
          <Label for="date_fin">Date de fin</Label>
          <Input
            type="text"
            name="date_fin"
            id="date_fin"
            onChange={this.onChange}
            value={this.state.date_fin === null ? "" : this.state.date_fin}
          />
        </FormGroup>
        <FormGroup>
          <Label for="importance">Importance</Label>
          <Input
            type="importance"
            name="importance"
            id="importance"
            onChange={this.onChange}
            value={this.state.importance === null ? "" : this.state.importance}
          />
        </FormGroup>
        <FormGroup>
          <Label for="etat">Etat</Label>
          <Input
            type="text"
            name="etat"
            id="etat"
            onChange={this.onChange}
            value={this.state.etat === null ? "" : this.state.etat}
          />
        </FormGroup>
        <Button color="primary">Envoyer</Button>
      </Form>
    );
  }
}

export default AddEditForm;
