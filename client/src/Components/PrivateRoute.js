import React from "react";
import API from "../utils/API.js";
import { Navigate, Outlet } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const auth = API.isAuth();
  return auth ? <Outlet /> : <Navigate to="/" />;
};
