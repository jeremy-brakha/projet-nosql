import React from "react";
import { Label, Alert, Button, FormGroup, Input } from "reactstrap";
import API from "../../utils/API";

export class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      visible: false,
      showApp: false,
      showSignup: false,
    };
    this.onDismiss = this.onDismiss.bind(this);
  }
  onDismiss() {
    this.setState({ visible: false });
  }

  send = async () => {
    const { email, password } = this.state;
    if (!email || email.length === 0) {
      return;
    }
    if (!password || password.length === 0) {
      return;
    }
    try {
      const { data } = await API.login(email, password);
      localStorage.setItem("token", data.token);
      window.location = "/app";
      this.setState({ showApp: true });
    } catch (error) {
      this.setState({
        visible: true,
      });
    }
  };
  
  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };
  render() {
    const data = {
      id: "1",
      title: "Un titre",
    };
    const { email, password, visible } = this.state;
    return (
      <div className="Login">
        <FormGroup style={{ marginBottom: "10px" }}>
          <Label for="email">Email</Label>
          <Input
            autoFocus
            type="email"
            id="email"
            value={email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup style={{ marginBottom: "10px" }}>
          <Label for="password">Mot de passe</Label>
          <Input
            value={password}
            onChange={this.handleChange}
            id="password"
            type="password"
          />
        </FormGroup>
        <Button
          style={{ marginTop: "10px", marginBottom: "10px" }}
          color="primary"
          onClick={this.send}
          type="submit"
        >
          Connexion
        </Button>{" "}
        <Button
          style={{ marginTop: "10px", marginBottom: "10px" }}
          color="primary"
          onClick={() => window.location = "/signup" }F
        >
          Créer un compte
        </Button>
        <Alert color="danger" isOpen={visible} toggle={this.onDismiss}>
          Email ou mot de passe incorrect.
        </Alert>
      </div>
    );
  }
}
