import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Test from "./Test"
import { Login } from "./Components/Login/login";
import { BrowserRouter } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <Test />
  </BrowserRouter>,
  document.getElementById("root")
);