import axios from "axios";
const headers = {
  "Content-Type": "application/json",
};
const burl = "http://localhost:8800";

export default {
  login: function (email, password) {
    return axios.post(
      `${burl}/user/login`,
      {
        email,
        password,
      },
      {
        headers: headers,
      }
    );
  },
  signup: function (email, password) {
    try {
      return axios.post(
        `${burl}/user/signup`,
        {
          email,
          password,
        },
        { headers: headers }
      );
    } catch (error) {
      console.log(error);
    }
  },

  // signup: async function () {
  //   return await axios.post(
  //     "http://localhost:8800/user/signup",
  //     {
  //       email: "jrmbrakha@gmail.com",
  //       password: "test",
  //     },
  //     {
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //     }
  //   );
  // },
  // signup: async function (send) {
  //   console.log(send);
  //   await fetch(`${burl}/user/signup`, {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify(send),
  //   })
  //     .then((response) => {
  //       console.log(response);
  //       response.json();
  //     })
  //     .then((item) => {
  //       console.log(item);
  //       return item;
  //     });
  // },

  isAuth: function () {
    return localStorage.getItem("token") !== null;
  },
  logout: function () {
    localStorage.clear();
  },
};
