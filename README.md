# Projet NoSQL

## Membres du groupe

* Cédric NAGNONHOU
* Jérémy BRAKHA

## But du projet

Le but de ce projet est de constituer une application web simple, avec au moins une page de formulaire qui puisse récupérer des données écrites par un utilisateur et qui les persiste dans deux systèmes différents : postgresql et mongodb.

## Comment lancer le projet

* Cloner le projet
* Se placer dans la branche main
* Lancer le docker-compose : docker-compose up -d --build
* Vérifier depuis Docker Desktop ou en ligne de commande (docker logs `<nom-conteneur>`) que les différents conteneurs tournent bien et surtout que les connexions aux bases de données sont faites, sinon il sera impossible de créer un compte. Si un échec à une base de données à eu lieu, relancer le conteneur.
* Aller à l'adresse suivante : http://localhost:3000/
