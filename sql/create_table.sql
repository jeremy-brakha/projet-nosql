CREATE TABLE taches (
    id serial PRIMARY KEY,
    description VARCHAR(100),
    date_fin VARCHAR(100),
    importance VARCHAR(100),
    etat VARCHAR(100),
    added TIMESTAMP NOT NULL
);